#include <omp.h>
#include "algebra.h"
#include <stdio.h>

void axpby(double* x, double* y, double a, double b, int n) {
    for(int i=0;i<n;i++){
        y[i]= x[i]*a + y[i]*b;
    }
}

void axpby_openmp(double* x, double* y, double a, double b, int n) {
    int i=0;
    #pragma omp parallel for shared(i,x,y,a,b,n) schedule(static)
    for(i=0;i<n;i++){
        y[i]= x[i]*a + y[i]*b;
    }
    
}

float dot (int n, float *u, float *v, int inc) {
    int result=0;
    for(int i=0;i<n;i+=inc){
        result+=u[i]*v[i];
    }
}

float dot_openmp (int n, float *u, float *v, int inc) {
    int result=0;
    int i=0;
    #pragma omp parallel for shared(i,u,v) schedule(static) reduction(+:result)
    for(i=0;i<n;i+=inc){
        result+=u[i]*v[i];
    }
}

struct Size {
    long double value;
    char* unit;
};

struct Size conv_unit(long int data_size){
    long double data=data_size;
    char* unit[]={"B","KB","MB","GB","TB"};
    int i = 0;
    while(data/1000>1 && i<5){
        data=data/1000;
        i++;
    }    
    struct Size s = {data, unit[i]};
    return s;
}

void print_stats(double start, double end, long int data_size) {    
    struct Size s1 = conv_unit(data_size);
    double total_time = end - start;
    printf("Data: %.2Lf %s\n", s1.value, s1.unit);
    printf("Time: %f sec\n", total_time);
    s1 = conv_unit(data_size/total_time);
    printf("Throughput: %Lf %s/s\n", s1.value, s1.unit);
}