#!/bin/bash

# Configuration for 1 node, 4 cores and 30 seconds of execution time
#SBATCH --job-name=ex1
#SBATCH --workdir=.
#SBATCH --output=algebra_%j.out
#SBATCH --error=algebra_%j.err
#SBATCH --cpus-per-task=4
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --time=00:00:30

source /shared/profiles.d/easybuild.sh
module load GCC/8.3.0

make || exit 1      # Exit if make fails
./algebra 100000000

