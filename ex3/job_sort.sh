#!/bin/bash

# Configuration for 1 node, 4 cores and 30 seconds of execution time
#SBATCH --job-name=ex3
#SBATCH --workdir=.
#SBATCH --output=sort_%j.out
#SBATCH --error=sort_%j.err
#SBATCH --cpus-per-task=4
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --time=00:00:30

source /shared/profiles.d/easybuild.sh  # init modules
module load GCC/8.3.0       # load module GCC

make || exit 1      # Exit if make fails
./sort 200000
