#include <time.h>
#include <stdlib.h>
#include <omp.h>
#include "sort.h"


int sort(int *array, int n) {
    int i, j, tmp;

    for (i=1;i<n;i++) {  
        tmp = array[i];  
        for(j=i-1;j >= 0 && array[j] > tmp;j--) {  
            array[j+1] = array[j];  
        }  
        array[j+1] = tmp;  
    }
}

int sort_openmp(int *array, int n) {
    int i, j, tmp;
    int chunk_size = n/_NUM_THREADS;
    int tmp_array[n];

    #pragma omp parallel for shared(i)
    for (i=0;i<n;i++) { 
        tmp_array[i]=array[i];
    }

    #pragma omp parallel shared(tmp_array) private(i,j,tmp) num_threads(_NUM_THREADS)
    {        
        int id = omp_get_thread_num();        
        for (i=1+chunk_size*id;i<(chunk_size*(id+1));i++) {  
            tmp = tmp_array[i];  
            for(j=i-1;j >= chunk_size*id && tmp_array[j] > tmp;j--) {  
                tmp_array[j+1] = tmp_array[j];  
            }  
            tmp_array[j+1] = tmp;  
        }
    }

    int head_i[_NUM_THREADS];
    for(i=0; i < _NUM_THREADS; i++){
        head_i[i]=chunk_size*i;
    }
   
    for (i=0;i<n;i++) { 
        int min = 2147483647;
        int min_index = 0;
        for (int j=0;j<_NUM_THREADS;j++) {
            if(head_i[j]<chunk_size*(j+1)){                
                if(tmp_array[head_i[j]]<min){
                    min=tmp_array[head_i[j]];
                    min_index=j;
                }
            }
        }
        array[i]=tmp_array[head_i[min_index]];
        head_i[min_index]++;
    }
}

void fill_array(int *array, int n) {
    int i;
    
    srand(time(NULL));
    for(i=0; i < n; i++) {
        array[i] = rand()%n;
    }
}
