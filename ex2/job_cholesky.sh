#!/bin/bash

# Configuration for 1 node, 4 cores and 30 seconds of execution time
#SBATCH --job-name=ex2
#SBATCH --workdir=.
#SBATCH --output=cholesky_%j.out
#SBATCH --error=cholesky_%j.err
#SBATCH --cpus-per-task=2
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --time=00:05:00

source /shared/profiles.d/easybuild.sh
module load GCC/8.3.0

make || exit 1      # Exit if make fails
./cholesky 3000
# ./cholesky 1000
