#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include <time.h>
#include "cholesky.h"



void cholesky_openmp(int n) {
    int i, j, k;
    double** A;
    double** L;
    double** U;
    double** B;
    double tmp,tmp2;
    double start, end;
    int cnt;
    
    /**
     * 1. Matrix initialization for A, L, U and B
     */
    start = omp_get_wtime();
    A = (double **)malloc(n * sizeof(double *)); 
    L = (double **)malloc(n * sizeof(double *)); 
    U = (double **)malloc(n * sizeof(double *)); 
    B = (double **)malloc(n * sizeof(double *)); 
    
    for(i=0; i<n; i++) {
         A[i] = (double *)malloc(n * sizeof(double)); 
         L[i] = (double *)malloc(n * sizeof(double)); 
         U[i] = (double *)malloc(n * sizeof(double)); 
         B[i] = (double *)malloc(n * sizeof(double)); 
    }
    
    srand(time(NULL));
    for(i=0; i < n; i++) {
        A[i][i] = rand()%1000+100.0;
        for(j=i+1; j < n; j++) {
            A[i][j] = rand()%100 + 1.0;
            A[j][i] = A[i][j];
        }
    }
    
    for(i=0; i < n; i++) {
        for(j=0; j < n; j++) {
            L[i][j] = 0.0;
            U[i][j] = 0.0;
        }
    }
    end = omp_get_wtime();
    printf("Initialization: %f\n", end-start);
    
    
    /**
     * 2. Compute Cholesky factorization for U
     */
    start = omp_get_wtime();
    for(i=0; i<n; i++) {
        // Calculate diagonal elements
        tmp = 0.0;
        #pragma omp parallel for reduction(+:tmp) schedule(static)
        for(k=0;k<i;k++) {
            tmp += U[k][i]*U[k][i];
        }
        U[i][i] = sqrt(A[i][i]-tmp);
        // Calculate non-diagonal elements
        // #pragma omp parallel for schedule(static)
        for(j=i+1;j<n;j++) { 
            tmp2 = 0.0;    
            // #pragma omp reduction(+:tmp2) schedule(static)
            #pragma omp parallel for reduction(+:tmp2) schedule(static)
            for(k=0;k<i;k++) {
                tmp2 += U[k][j]*U[k][i];
            }
            U[i][j] = (A[j][i] - tmp2) / U[i][i]; 
        }
    }
    end = omp_get_wtime();
    printf("Cholesky: %f\n", end-start);
    
        
    /**
     * 3. Calculate L from U'
     */
    start = omp_get_wtime();
    #pragma omp parallel for shared(i,j)
    for(i=0; i<n; i++) {
        for(j=0; j<n; j++) {
            L[j][i]=U[i][j];
        }
    }
    end = omp_get_wtime();
    printf("L=U': %f\n", end-start);
    
    
    /**
     * 4. Compute B=LU
     */
    start = omp_get_wtime();
    #pragma omp parallel for private(i,j,tmp) 
    for (i = 0; i < n; i++) {
      for (j = 0; j < n; j++) {
        tmp = 0.0;
        for (k = 0; k <= i; k++) {
            tmp += L[i][k]*U[k][j];
        } 
        B[i][j] = tmp;
      }
    }
    end = omp_get_wtime();
    printf("B=LU: %f\n", end-start);


    /**
     * 5. Check if all elements of A and B have a difference smaller than 0.001%
     */
    start = omp_get_wtime();
    cnt=0;
    #pragma omp parallel for
    for (i = 0; i < n; i++) {
      for (j = 0; j < n; j++) {
        if (abs((A[i][j] - B[i][j])) > 0.001)
        {
          cnt += 1;
        }
      }
    }
    if(cnt != 0) {
        printf("Matrices are not equal\n");
    } else {
        printf("Matrices are equal\n");
    }
    end = omp_get_wtime();
    printf("A==B?: %f\n", end-start);   
}


void cholesky(int n) {
    int i, j, k;
    double** A;
    double** L;
    double** U;
    double** B;
    double tmp;
    double start, end;
    int cnt;
    
    /**
     * 1. Matrix initialization for A, L, U and B
     */
    start = omp_get_wtime();
    A = (double **)malloc(n * sizeof(double *)); 
    L = (double **)malloc(n * sizeof(double *)); 
    U = (double **)malloc(n * sizeof(double *)); 
    B = (double **)malloc(n * sizeof(double *)); 
    
    for(i=0; i<n; i++) {
         A[i] = (double *)malloc(n * sizeof(double)); 
         L[i] = (double *)malloc(n * sizeof(double)); 
         U[i] = (double *)malloc(n * sizeof(double)); 
         B[i] = (double *)malloc(n * sizeof(double)); 
    }
    
    srand(time(NULL));
    for(i=0; i < n; i++) {
        A[i][i] = rand()%1000+100.0;
        for(j=i+1; j < n; j++) {
            A[i][j] = rand()%100 + 1.0;
            A[j][i] = A[i][j];
        }
    }

    for(i=0; i < n; i++) {
        for(j=0; j < n; j++) {
            L[i][j] = 0.0;
            U[i][j] = 0.0;
        }
    }
    end = omp_get_wtime();
    printf("Initialization: %f\n", end-start);
    
    
    /**
     * 2. Compute Cholesky factorization for U
     */
    start = omp_get_wtime();
    for(i=0; i<n; i++) {
        // Calculate diagonal elements
        tmp = 0.0;
        for(k=0;k<i;k++) {
            tmp += U[k][i]*U[k][i];
        }
        U[i][i] = sqrt(A[i][i]-tmp);
        // Calculate non-diagonal elements
        for(j=i+1;j<n;j++) {
            tmp = 0.0;
            for(k=0;k<i;k++) {
                tmp += U[k][j]*U[k][i];
            }
            U[i][j] = (A[j][i] - tmp) / U[i][i]; 
        }
    }
    end = omp_get_wtime();
    printf("Cholesky: %f\n", end-start);
    
        
    /**
     * 3. Calculate L from U'
     */
    start = omp_get_wtime();
    for(i=0; i<n; i++) {
        for(j=0; j<n; j++) {
            L[j][i]=U[i][j];
        }
    }
    end = omp_get_wtime();
    printf("L=U': %f\n", end-start);
    
    
    /**
     * 4. Compute B=LU
     */
    start = omp_get_wtime();
    for (i = 0; i < n; i++) {
      for (j = 0; j < n; j++) {
        tmp = 0.0;
        for (k = 0; k <= i; k++) {
            tmp += L[i][k]*U[k][j];
        } 
        B[i][j] = tmp;
      }
    }
    end = omp_get_wtime();
    printf("B=LU: %f\n", end-start);


    /**
     * 5. Check if all elements of A and B have a difference smaller than 0.001%
     */
    start = omp_get_wtime();
    cnt=0;
    for (i = 0; i < n; i++) {
      for (j = 0; j < n; j++) {
        if (abs((A[i][j] - B[i][j])) > 0.001)
        {
          cnt += 1;
        }
      }
    }
    if(cnt != 0) {
        printf("Matrices are not equal\n");
    } else {
        printf("Matrices are equal\n");
    }
    end = omp_get_wtime();
    printf("A==B?: %f\n", end-start);
}


